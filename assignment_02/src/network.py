from mnist import *
import tensorflow as tf
import sys
import time

class Network:
    def __init__(self, device, num_filters):
        """
        Define network.
        """
        with tf.device(device):
            x = tf.placeholder(tf.float32, shape=[None, 784])  # input: arbitrary batch size, 28x28pxl
            y = tf.placeholder(tf.float32, shape=[None, 10])  # output: arbitrary batch size, 10 classes

            def weight_matrix(shape):
              initial = tf.truncated_normal(shape, stddev=0.1)
              return tf.Variable(initial)

            def bias_vector(shape):
              initial = tf.constant(0.1, shape=shape)
              return tf.Variable(initial)

            def conv2d(x, W):
              return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

            def max_pool_2x2(x):
              return tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')

            x_image = tf.reshape(x, [-1, 28, 28, 1])

            # first convolutional layer
            W_conv1 = weight_matrix([3, 3, 1, num_filters])  # 3x3 filters of depth 1 (greyscale)
            b_conv1 = bias_vector([num_filters])

            out_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)

            # first max_pool
            out_pool1 = max_pool_2x2(out_conv1)

            # second convolutional layer
            W_conv2 = weight_matrix([3, 3, num_filters, num_filters])  # 3x3 filters of depth num_filters
            b_conv2 = bias_vector([num_filters])

            out_conv2 = tf.nn.relu(conv2d(out_pool1, W_conv2) + b_conv2)

            # second max_pool
            out_pool2 = max_pool_2x2(out_conv2)  # input is downsampled to 28/2/2= 7x7
            out_pool2_flat = tf.reshape(out_pool2, [-1, 7 * 7 * num_filters])  # reshape to flat vector

            # first fully conected layer
            W_fcon1 = weight_matrix([7 * 7 * num_filters, 128])  # 128 units
            b_fcon1 = bias_vector([128])

            out_fcon1 = tf.nn.relu(tf.matmul(out_pool2_flat, W_fcon1) + b_fcon1)

            # second fully conected layer
            W_fcon2 = weight_matrix([128, 10])  # 10 units
            b_fcon2 = bias_vector([10])

            y_pred = tf.nn.relu(tf.matmul(out_fcon1, W_fcon2) + b_fcon2)

            self.y_pred = y_pred
            self.x = x
            self.y = y

    def train(self, device, learning_rate, keep_time):
        """
        Train and evaluate network.
        """
        mnist = Mnist()
        print("... training")
        with tf.device(device):
            cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=self.y, logits=self.y_pred))
            train_step = tf.train.GradientDescentOptimizer(learning_rate).minimize(cross_entropy)
            correct_prediction = tf.equal(tf.argmax(self.y, 1), tf.argmax(self.y_pred, 1))
            accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

            Dtrain = mnist.getTrainData()
            Dval = mnist.getValData()
            num_samples = 50000
            batch_size = 100
            data = [] # stores validation accuracy for each epoch
            with tf.Session() as sess:
              sess.run(tf.global_variables_initializer())
              start_time = time.time()
              for e in range(10):  # train for 10 epochs
                for i in range(0, num_samples, batch_size):
                  batch = (Dtrain[0][i : batch_size + i], Dtrain[1][i : batch_size + i])
                  sess.run(train_step, feed_dict={self.x: batch[0], self.y: batch[1]})
                  if not keep_time:
                    v_acc = 1 - sess.run(accuracy, feed_dict={self.x: Dval[0], self.y: Dval[1]})
                    data.append(v_acc)
                    print("validation error: %.2f" % v_acc)
                end_time = time.time()
                if not keep_time:
                  print("accuracy: %.2f" % sess.run(accuracy, feed_dict={self.x: Dtest[0], self.y: Dtest[1]}))
                  with open("../data/results.txt", 'w') as rfile:
                      for val in data:
                          rfile.write("%s\n" % val)

              else:
                  print("time: %.2f" % (end_time - start_time))

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Call with four parameter:")
        print("[bool]: measure time to train the network")
        print("[str]: device, i.e /gpu:0")
        print("[int]: number of filter")
        print("[float]: learning rate")
        sys.exit()

    keep_time = bool(sys.argv[1])
    device = str(sys.argv[2])
    num_filters = int(sys.argv[3])
    learning_rate = float(sys.argv[4])

    nn = Network(device, num_filters)
    nn.train(device, learning_rate, keep_time)
