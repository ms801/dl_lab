import matplotlib.pyplot as plt
import sys
import math

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Call with one parameter:")
        print("[str]: path to time.txt")
        sys.exit()

    path = str(sys.argv[1])

    filter = []
    for i in range(4):
        f = math.pow(2, i+3)
        params = 9 * f + 9 * f * f + 49 * f * 128 + 1280
        filter.append(params)
    for i in range(6):
        f = math.pow(2, i+3)
        params = 9 * f + 9 * f * f + 49 * f * 128 + 1280
        filter.append(params)

    time = []
    with open(path) as rfile:
        input = rfile.read().split("\n")
        for x in input:
            time.append(float(x))
    time_cpu = time[0:4]
    time_gpu = time[4:]

    plt.plot(filter[0:4], time_cpu, 'x', label="time cpu")
    plt.plot(filter[4:], time_gpu, '*', label="time gpu")
    plt.legend()
    plt.xlabel('number of parameters')
    plt.ylabel('required time to train network [seconds]')
    plt.show()
