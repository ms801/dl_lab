import pickle
import os
import gzip
import time
from network import *
import numpy as np

class Mnist:
    """
    Class to load and store the MNIST datasat.
    """
    def __init__(self):
        Dtrain, Dval, Dtest = self.load()
        X_train, Y_train = Dtrain
        X_val, Y_val = Dval
        X_test, Y_test = Dtest
        self.X_train = X_test
        self.X_val = X_val
        self.X_test = X_test
        self.X_train = X_train.reshape(X_train.shape[0], -1)  # reshape to flat vector
        self.X_val = X_val.reshape(X_val.shape[0], -1)
        self.X_test = X_test.reshape(X_test.shape[0], -1)
        self.Y_train = self.one_hot(Y_train)
        self.Y_val = self.one_hot(Y_val)
        self.Y_test = self.one_hot(Y_test)

    def getTrainData(self):
        return (self.X_train, self.Y_train)

    def getValData(self):
        return (self.X_val, self.Y_val)

    def getTestData(self):
        return (self.X_test, self.Y_test)

    def load(self, datasets_dir='../data'):
        """
        Loads the MNIST dataset from the web and stores it in /data.
        Returns train, validation and test set.
        """
        if not os.path.exists(datasets_dir):
            os.mkdir(datasets_dir)
        data_file = os.path.join(datasets_dir, 'mnist.pkl.gz')
        if not os.path.exists(data_file):
            print('... downloading MNIST from the web')
            try:
                import urllib
                urllib.urlretrieve('http://google.com')
            except AttributeError:
                import urllib.request as urllib
            url = 'http://www.iro.umontreal.ca/~lisa/deep/data/mnist/mnist.pkl.gz'
            urllib.urlretrieve(url, data_file)

        print('... loading data')
        # Load the dataset
        f = gzip.open(data_file, 'rb')
        try:
            train_set, valid_set, test_set = pickle.load(f, encoding="latin1")
        except TypeError:
            train_set, valid_set, test_set = pickle.load(f)
        f.close()

        test_x, test_y = test_set
        test_x = test_x.astype('float32')
        test_x = test_x.astype('float32').reshape(test_x.shape[0], 1, 28, 28)
        test_y = test_y.astype('int32')
        valid_x, valid_y = valid_set
        valid_x = valid_x.astype('float32')
        valid_x = valid_x.astype('float32').reshape(valid_x.shape[0], 1, 28, 28)
        valid_y = valid_y.astype('int32')
        train_x, train_y = train_set
        train_x = train_x.astype('float32').reshape(train_x.shape[0], 1, 28, 28)
        train_y = train_y.astype('int32')
        rval = [(train_x, train_y), (valid_x, valid_y), (test_x, test_y)]
        print('... done loading data')
        return rval

    def one_hot(self, labels):
        """ This creates a one hot encoding from a flat vector.

        >>> print(one_hot(np.array([0, 2, 1])))
        [[ 1.  0.  0.]
         [ 0.  0.  1.]
         [ 0.  1.  0.]]
        """
        classes = np.unique(labels)
        n_classes = classes.size
        one_hot_labels = np.zeros(labels.shape + (n_classes,))
        for c in classes:
            one_hot_labels[labels == c, c] = 1
        return one_hot_labels
