import matplotlib.pyplot as plt
import sys

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Call with one parameter:")
        print("[str]: path to data folder containing result_01.txt until result_0001.txt")
        sys.exit()

    path = str(sys.argv[1])

    epoch = [i+1 for i in range(10)]
    v_acc1 = []
    v_acc2 = []
    v_acc3 = []
    v_acc4 = []
    with open(path + "/result_1.txt") as rfile:
        input = rfile.read().split("\n")
        for x in input:
            v_acc1.append(float(x))
    with open(path + "/result_01.txt") as rfile:
        input = rfile.read().split("\n")
        for x in input:
            v_acc2.append(float(x))
    with open(path + "/result_001.txt") as rfile:
        input = rfile.read().split("\n")
        for x in input:
            v_acc3.append(float(x))
    with open(path + "/result_0001.txt") as rfile:
        input = rfile.read().split("\n")
        for x in input:
            v_acc4.append(float(x))

    plt.plot(epoch, v_acc1, label="rate = 0.1")
    plt.plot(epoch, v_acc2, label="rate = 0.01")
    plt.plot(epoch, v_acc3, label="rate = 0.001")
    plt.plot(epoch, v_acc4, label="rate = 0.0001")
    plt.legend()
    plt.xlabel('epochs')
    plt.ylabel('validation_error')
    plt.show()
