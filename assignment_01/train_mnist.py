import numpy as np
import cPickle
import os
import gzip
import time
from network import *

def mnist(datasets_dir='./data'):
    if not os.path.exists(datasets_dir):
        os.mkdir(datasets_dir)
    data_file = os.path.join(datasets_dir, 'mnist.pkl.gz')
    if not os.path.exists(data_file):
        print('... downloading MNIST from the web')
        try:
            import urllib
            urllib.urlretrieve('http://google.com')
        except AttributeError:
            import urllib.request as urllib
        url = 'http://www.iro.umontreal.ca/~lisa/deep/data/mnist/mnist.pkl.gz'
        urllib.urlretrieve(url, data_file)

    print('... loading data')
    # Load the dataset
    f = gzip.open(data_file, 'rb')
    try:
        train_set, valid_set, test_set = cPickle.load(f, encoding="latin1")
    except TypeError:
        train_set, valid_set, test_set = cPickle.load(f)
    f.close()

    test_x, test_y = test_set
    test_x = test_x.astype('float32')
    test_x = test_x.astype('float32').reshape(test_x.shape[0], 1, 28, 28)
    test_y = test_y.astype('int32')
    valid_x, valid_y = valid_set
    valid_x = valid_x.astype('float32')
    valid_x = valid_x.astype('float32').reshape(valid_x.shape[0], 1, 28, 28)
    valid_y = valid_y.astype('int32')
    train_x, train_y = train_set
    train_x = train_x.astype('float32').reshape(train_x.shape[0], 1, 28, 28)
    train_y = train_y.astype('int32')
    rval = [(train_x, train_y), (valid_x, valid_y), (test_x, test_y)]
    print('... done loading data')
    return rval

# Download dataset
Dtrain, Dval, Dtest = mnist()
X_train, Y_train = Dtrain
X_valid, Y_valid = Dval
# Downsample training data to make it a bit faster for testing this code
n_train_samples = 10000
train_idxs = np.random.permutation(X_train.shape[0])[:n_train_samples]
X_train = X_train[train_idxs]
Y_train = Y_train[train_idxs]

print("X_train shape: {}".format(np.shape(X_train)))
print("Y_train shape: {}".format(np.shape(Y_train)))

X_train = X_train.reshape(X_train.shape[0], -1)
print("Reshaped X_train size: {}".format(X_train.shape))
X_valid = X_valid.reshape(X_valid.shape[0], -1)
print("Reshaped X_valid size: {}".format(X_valid.shape))


import time

# Setup a small MLP / Neural Network
# we can set the first shape to None here to indicate that
# we will input a variable number inputs to the network
input_shape = (None, 28*28)
layers = [InputLayer(input_shape)]
layers.append(FullyConnectedLayer(
                layers[-1],
                num_units=500,
                init_stddev=0.01,
                activation_fun=Activation('relu')
))
layers.append(FullyConnectedLayer(
                layers[-1],
                num_units=10,
                init_stddev=0.01,
                # last layer has no nonlinearity
                # (softmax will be applied in the output layer)
                activation_fun=None
))
layers.append(SoftmaxOutput(layers[-1]))
nn = NeuralNetwork(layers)

# Train neural network
t0 = time.time()
nn.train(X_train, Y_train, X_valid, Y_valid, learning_rate=0.3,
         max_epochs=15, batch_size=20, descent_type="sgd", y_one_hot=True)
t1 = time.time()
print('Duration: {:.1f}s'.format(t1-t0))


# Print results
print("Testing...")
X_test, Y_test = Dtest
X_test = X_test.reshape(X_test.shape[0], -1)

classification_error = nn.classification_error(X_test, Y_test)
print("classification_error = {}".format(classification_error))

# Visualize images
Y_pred = unhot(nn.predict(X_test))
error = Y_pred != Y_test

w_id_array = np.where(error == 1)[0]
c_id_array = np.where(error == 0)[0]

import matplotlib.pyplot as plt

# Missclassified
for i in range(5):
    w_id = w_id_array[i]
    x = X_test[w_id]
    y = Y_test[w_id]
    y_pred = Y_pred[w_id]

    plt.imshow(np.reshape(x, (28, 28)), cmap='gray')
    plt.title("Missclassified " + str(y) + " as " + str(y_pred))
    plt.show()

# Correctly classified
for i in range(5):
    c_id = c_id_array[i]
    x = X_test[c_id]
    y = Y_test[c_id]
    y_pred = Y_pred[c_id]
    plt.imshow(np.reshape(x, (28, 28)), cmap='gray')
    plt.title("Correctly classified as " + str(y))
    plt.show()
