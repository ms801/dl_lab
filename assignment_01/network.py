import numpy as np

# Neural network activation functions and their derivatives
def sigmoid(x):
    return 1.0/(1.0+np.exp(-x))

def sigmoid_d(x):
    return sigmoid(x) * (1 - sigmoid(x))

def tanh(x):
    return np.tanh(x)

def tanh_d(x):
    return 1 - np.power(tanh(x), 2)

def relu(x):
    return np.maximum(0.0, x)

def relu_d(x):
    """
    >>> print(relu_d(np.array([-1, 0, 1, 2])))
    [0 0 1 1]
    """
    return np.where(x > 0, 1, 0)

# Helper functions
def softmax(x, axis=1):
    """ Computes a categorical distribution.

    >>> print(softmax(np.array([[1 ,1, 1, 1], [5 ,2, 1, 1]])))
    [[ 0.25        0.25        0.25        0.25      ]
     [ 0.92045574  0.04582679  0.01685873  0.01685873]]
    """

    # To make the softmax a "safe" operation we will
    # first subtract the maximum along the specified axis
    # so that np.exp(x) does not blow up!
    # Note that this does not change the output.
    x_max = np.max(x, axis=axis, keepdims=True)
    x_safe = x - x_max
    e_x = np.exp(x_safe)
    return e_x / np.sum(e_x, axis=axis, keepdims=True)

def one_hot(labels):
    """ This creates a one hot encoding from a flat vector.

    >>> print(one_hot(np.array([0, 2, 1])))
    [[ 1.  0.  0.]
     [ 0.  0.  1.]
     [ 0.  1.  0.]]
    """
    classes = np.unique(labels)
    n_classes = classes.size
    one_hot_labels = np.zeros(labels.shape + (n_classes,))
    for c in classes:
        one_hot_labels[labels == c, c] = 1
    return one_hot_labels

def unhot(one_hot_labels):
    """ Invert a one hot encoding, creating a flat vector.

    >>> print(unhot(np.array([[1, 0, 0], [0, 0, 1], [0, 1, 0]])))
    [0 2 1]
    """
    return np.argmax(one_hot_labels, axis=-1)

# Activation function class
class Activation(object):
    """
    Computes output (fprop), backpropagated value(bprop).
    Stores activation function (act), last input (last_input).
    """

    def __init__(self, tname):
        if tname == 'sigmoid':
            self.act = sigmoid
            self.act_d = sigmoid_d
        elif tname == 'tanh':
            self.act = tanh
            self.act_d = tanh_d
        elif tname == 'relu':
            self.act = relu
            self.act_d = relu_d
        else:
            raise ValueError('Invalid activation function.')

    def fprop(self, input):
        # we need to remember the last input
        # so that we can calculate the derivative with respect
        # to it later on
        self.last_input = input
        return self.act(input)

    def bprop(self, output_grad):
        return output_grad * self.act_d(self.last_input)

# Base class for layers (interface)
class Layer(object):

    def fprop(self, input):
        """ Calculate layer output for given input
            (forward propagation).
        """
        raise NotImplementedError('This is an interface class, please use a derived instance')

    def bprop(self, output_grad):
        """ Calculate input gradient and gradient
            with respect to weights and bias (backpropagation).
        """
        raise NotImplementedError('This is an interface class, please use a derived instance')

    def output_size(self):
        """ Calculate size of this layer's output.
        input_shape[0] is the number of samples in the input.
        input_shape[1:] is the shape of the feature.
        """
        raise NotImplementedError('This is an interface class, please use a derived instance')

# Base class for loss outputs
# an output layer can then simply be derived
# from both Layer and Loss
class Loss(object):

    def loss(self, output, output_net):
        """ Calculate mean loss given real output and network output. """
        raise NotImplementedError('This is an interface class, please use a derived instance')

    def input_grad(self, output, output_net):
        """ Calculate input gradient real output and network output. """
        raise NotImplementedError('This is an interface class, please use a derived instance')

# Base class for parameterized things
class Parameterized(object):

    def params(self):
        """ Return parameters (by reference) """
        raise NotImplementedError('This is an interface class, please use a derived instance')

    def grad_params(self):
        """ Return accumulated gradient with respect to params. """
        raise NotImplementedError('This is an interface class, please use a derived instance')

# Container for providing input to the network
class InputLayer(Layer):

    def __init__(self, input_shape):
        if not isinstance(input_shape, tuple):
            raise ValueError("InputLayer requires input_shape as a tuple")
        self.input_shape = input_shape

    def output_size(self):
        return self.input_shape

    def fprop(self, input):
        return input

    def bprop(self, output_grad):
        return output_grad

# Hidden Layer
class FullyConnectedLayer(Layer, Parameterized):
    """ A standard fully connected hidden layer, as discussed in the lecture.
    """

    def __init__(self, input_layer, num_units,
                 init_stddev, activation_fun=Activation('relu')):
        """
        >>> fLayer = FullyConnectedLayer(input_layer=InputLayer((1, 3)), num_units=2, init_stddev=0.01)
        >>> params = fLayer.params()
        >>> print(params[0].shape) # Shape of matrix W
        (3, 2)
        >>> print(params[1].shape) # Shape of vector b
        (2,)
        """

        self.num_units = num_units
        self.activation_fun = activation_fun
        # the input shape will be of size (batch_size, num_units_prev)
        # where num_units_prev is the number of units in the input
        # (previous) layer
        self.input_shape = input_layer.output_size()
        # ################################
        # this is the weight matrix it should have shape: (num_units_prev, num_units)
        self.W = np.random.normal(loc=0.0, scale=init_stddev, size=(1, self.input_shape[1], self.num_units))[0]
        # and this is the bias vector of shape: (num_units)
        self.b = np.random.normal(loc=0.0, scale=init_stddev, size=(1, 1, num_units))[0][0]
        # ################################
        # create dummy variables for parameter gradients
        # no need to change these here!
        self.dW = None
        self.db = None

    def output_size(self):
        return (self.input_shape[0], self.num_units)

    def fprop(self, input):
        """
        >>> fLayer = FullyConnectedLayer(input_layer=InputLayer((4, 3)),
        ...              num_units=2, init_stddev=0.01, activation_fun=None)

        Input of batch_size = 4, num_units_prev = 3
        >>> X = np.array([[1, 1, 1], [2, 2, 2], [1, 1, 1], [2, 2, 2]])

        Expect output of batch_size = 4, num_units = 2
        >>> print(fLayer.fprop(X).shape)
        (4, 2)
        """
        # cache the last_input for the bprop
        self.last_input = input

        # ################################################
        a = input.dot(self.W) + self.b

        if self.activation_fun == None:
            return a
        else:
            return self.activation_fun.fprop(a)
        # ################################################

    def bprop(self, output_grad):
        """ Calculate input gradient (backpropagation). """
        # ################################
        d_act = output_grad
        if self.activation_fun != None:
            d_act = self.activation_fun.bprop(output_grad)
        grad_input = d_act.dot(self.W.transpose()) # the gradient wrt. the input
        self.dW = self.last_input.transpose().dot(d_act) # the gradient wrt. W
        self.db = np.sum(d_act, axis=0) # the gradient wrt. b
        # ###############################

        # divide the weights by n to make gradient checking work
        n = output_grad.shape[0]
        self.dW /= n
        self.db /= n
        return grad_input

    def params(self):
        return self.W, self.b

    def grad_params(self):
        return self.dW, self.db

# OutputLayer
class LinearOutput(Layer, Loss):
    """ A simple linear output layer that
        uses a squared loss (e.g. should be used for regression)
    """
    def __init__(self, input_layer):
        self.input_size = input_layer.output_size()

    def output_size(self):
        return (1,)

    def fprop(self, input):
        return input

    def bprop(self, output_grad):
        raise NotImplementedError(
            'LinearOutput should only be used as the last layer of a Network'
            + ' bprop() should thus never be called on it!'
        )

    def input_grad(self, Y, Y_pred):
        """
        >>> Y_pred = np.array([[1, 0, 0, 0], [0, 0, 0.5, 0.5]])
        >>> Y = np.array([[1, 0, 0, 0], [0, 0, 0.6, 0.4]])
        >>> lOut = LinearOutput(InputLayer((0, 0)))
        >>> print(lOut.input_grad(Y, Y_pred))
        [[ 0.   0.   0.   0. ]
         [ 0.   0.  -0.1  0.1]]
        """
        # ####################################
        # implement gradient of squared loss
        return Y_pred - Y
        # ####################################

    def loss(self, Y, Y_pred):
        loss = 0.5 * np.square(Y_pred - Y)
        return np.mean(np.sum(loss, axis=1))

class SoftmaxOutput(Layer, Loss):
    """ A softmax output layer that calculates
        the negative log likelihood as loss
        and should be used for classification.
    """

    def __init__(self, input_layer):
        self.input_size = input_layer.output_size()

    def output_size(self):
        return (1,)

    def fprop(self, input):
        return softmax(input)

    def bprop(self, output_grad):
        raise NotImplementedError(
            'SoftmaxOutput should only be used as the last layer of a Network'
            + ' bprop() should thus never be called on it!'
        )

    def input_grad(self, Y, Y_pred):
        """
        >>> Y_pred = np.array([[1, 0, 0, 0], [0, 0, 1, 0]])
        >>> Y = np.array([[1, 0, 0, 0], [1, 0, 0, 0]])
        >>> sOut = SoftmaxOutput(InputLayer((0, 0)))
        >>> print(sOut.input_grad(Y, Y_pred))
        [[ 0  0  0  0]
         [-1  0  1  0]]
        """
        # #######################################################
        # gradient of the negative log likelihood loss
        return -(Y - Y_pred)
       # #######################################################

    def loss(self, Y, Y_pred):
        """
        Output of batch_size = 2, num_units = 4
        >>> Y_pred = np.array([[1, 0, 0, 0], [0, 0, 1, 0]])
        >>> Y = np.array([[1, 0, 0, 0], [1, 0, 0, 0]])
        >>> sOut = SoftmaxOutput(InputLayer((0, 0)))
        >>> print(sOut.loss(Y, Y_pred))
        11.512925465
        """
        # ####################################
        # to make the loss numerically stable
        # you may want to add an epsilon in the log ;)
        eps = 1e-10
        loss = - np.log(np.maximum(Y_pred, eps)) * Y
        return np.mean(np.sum(loss, axis=1))
        # ####################################

class NeuralNetwork:
    """ Our Neural Network container class.
    """
    def __init__(self, layers):
        self.layers = layers

    def _loss(self, X, Y):
        Y_pred = self.predict(X)
        return self.layers[-1].loss(Y, Y_pred)

    def predict(self, X):
        """ Calculate an output Y for the given input X. """
        # ##########################################
        # forward pass through all layers
        Y_pred = self.layers[0].fprop(X)
        for i in range(1, len(self.layers)):
            Y_pred = self.layers[i].fprop(Y_pred)
        ##########################################
        return Y_pred

    def backpropagate(self, Y, Y_pred, upto=0):
        """ Backpropagation of partial derivatives through
            the complete network up to layer 'upto'
        """
        next_grad = self.layers[-1].input_grad(Y, Y_pred)
        # ##########################################
        # backward pass through all layers
        for i in range(2, len(self.layers) + 1 - upto):
            next_grad = self.layers[-i].bprop(next_grad)
        ##########################################
        return next_grad

    def classification_error(self, X, Y):
        """ Calculate error on the given data
            assuming they are classes that should be predicted.
        """
        Y_pred = unhot(self.predict(X))
        error = Y_pred != Y
        return np.mean(error)

    def sgd_epoch(self, X, Y, learning_rate, batch_size):
        """ Stochastic gradient decent.
        """
        n_samples = X.shape[0]
        n_batches = n_samples // batch_size
        for b in range(n_batches):
            # #####################################
            # extract a batch from X and Y
            start_index = b * batch_size
            X_b = X[start_index : start_index + batch_size]
            Y_b = Y[start_index : start_index + batch_size]

            Y_b_pred = self.predict(X_b) # fprop
            self.backpropagate(Y_b, Y_b_pred) # bprop

            # update weights
            for i in range(len(self.layers)):
                if isinstance(self.layers[i], Parameterized):
                    self.layers[i].W -= learning_rate * self.layers[i].dW
                    self.layers[i].b -= learning_rate * self.layers[i].db
            # #####################################

    def gd_epoch(self, X, Y, learning_rate):
        """ Batch gradient descent.
        """
        # #####################################
        Y_pred = self.predict(X) # fprop
        self.backpropagate(Y, Y_pred) #bprop

        # update weights
        for i in range(len(self.layers)):
            if isinstance(self.layers[i], Parameterized):
                self.layers[i].W -= learning_rate * self.layers[i].dW
                self.layers[i].b -= learning_rate * self.layers[i].db
        # #####################################

    def train(self, X, Y, X_valid, Y_valid, learning_rate=0.1, max_epochs=100,
              batch_size=64, descent_type="sgd", y_one_hot=True):
        """ Train network on the given data. """
        n_samples = X.shape[0]
        n_batches = n_samples // batch_size
        if y_one_hot:
            Y_train = one_hot(Y)
        else:
            Y_train = Y
        print("... starting training")
        for e in range(max_epochs+1):
            if descent_type == "sgd":
                self.sgd_epoch(X, Y_train, learning_rate, batch_size)
            elif descent_type == "gd":
                self.gd_epoch(X, Y_train, learning_rate)
            else:
                raise NotImplementedError("Unknown gradient descent type {}".format(descent_type))

            # Output error on the training data
            train_loss = self._loss(X, Y_train)
            train_error = self.classification_error(X, Y)
            print('epoch {:.4f}, loss {:.4f}, train error {:.4f}'.format(e, train_loss, train_error))

            # ##################################################
            # Output error on validation data
            valid_loss = self._loss(X_valid, one_hot(Y_valid))
            valid_error = self.classification_error(X_valid, Y_valid)
            print('epoch {:.0f}, loss {:.4f}, valid error {:.4f}'.format(e, valid_loss, valid_error))
            # ##################################################

    def check_gradients(self, X, Y):
        """ Helper function to test the parameter gradients for correctness.

        >>> input_shape = (5, 10)
        >>> n_labels = 6
        >>> layers = [InputLayer(input_shape)]
        >>> layers.append(FullyConnectedLayer(
        ...               layers[-1],
        ...               num_units=15,
        ...               init_stddev=0.1,
        ...               activation_fun=Activation('relu')))
        >>> layers.append(FullyConnectedLayer(
        ...               layers[-1],
        ...               num_units=6,
        ...               init_stddev=0.1,
        ...               activation_fun=Activation('tanh')))
        >>> layers.append(FullyConnectedLayer(
        ...               layers[-1],
        ...               num_units=n_labels,
        ...               init_stddev=0.1,
        ...               activation_fun=None))
        >>> layers.append(SoftmaxOutput(layers[-1]))
        >>> nn = NeuralNetwork(layers)

        create random data
        >>> X = np.random.normal(size=input_shape)

        and random labels
        >>> Y = np.zeros((input_shape[0], n_labels))
        >>> for i in range(Y.shape[0]):
        ...    idx = np.random.randint(n_labels)
        ...    Y[i, idx] = 1.
        >>> nn.check_gradients(X, Y) # doctest: +ELLIPSIS
        checking ...
        """
        for l, layer in enumerate(self.layers):
            if isinstance(layer, Parameterized):
                print('checking gradient for layer {}'.format(l))
                # we iterate through all parameters
                for p, param in enumerate(layer.params()):
                    # define functions for conveniently swapping
                    # out parameters of this specific layer and
                    # computing loss and gradient with these
                    # changed parametrs
                    def output_given_params(param_new):
                        """ A function that will compute the output
                            of the network given a set of parameters
                        """
                        # copy provided parameters
                        param[:] = np.reshape(param_new, param.shape)
                        # return computed loss
                        return self._loss(X, Y)

                    def grad_given_params(param_new):
                        """A function that will compute the gradient
                           of the network given a set of parameters
                        """
                        # copy provided parameters
                        param[:] = np.reshape(param_new, param.shape)
                        # Forward propagation through the net
                        Y_pred = self.predict(X)
                        # Backpropagation of partial derivatives
                        self.backpropagate(Y, Y_pred, upto=l)
                        # return the computed gradient
                        return np.ravel(self.layers[l].grad_params()[p])

                    # let the initial parameters be the ones that
                    # are currently placed in the network and flatten them
                    # to a vector for convenient comparisons, printing etc.
                    param_init = np.ravel(np.copy(param))

                    import scipy.optimize
                    # gradient as calculated through scipy
                    err_scipy = scipy.optimize.check_grad(output_given_params, grad_given_params, param_init)

                    epsilon = 1e-4

                    # gradient as calculated through bprop
                    gparam_bprop = grad_given_params(param_init)

                    # the gradient calculated through finite differences
                    gparam_fd = np.zeros_like(param_init)
                    loss_base = output_given_params(param_init)

                    for i in range(param_init.shape[0]):
                        param_eps = np.ravel(np.copy(param_init))
                        param_eps[i] += epsilon

                        loss_eps = output_given_params(param_eps)
                        gparam_fd[i] = (loss_eps - loss_base) / epsilon


                    # calculate difference between them
                    err = np.mean(np.abs(gparam_bprop - gparam_fd))
                    print('diffScipy {:.2e}'.format(err_scipy))
                    print('diff {:.2e}'.format(err))
                    assert(err < epsilon)

                    # reset the parameters to their initial values
                    param[:] = np.reshape(param_init, param.shape)
