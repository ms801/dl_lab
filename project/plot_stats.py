import matplotlib.pyplot as plt
import pickle

show = True

with open("./stats", 'rb') as f:
    stats = pickle.load(f)

episode_lengths = []
episode_rewards = []
for _, (l, r) in enumerate(stats):
    episode_lengths.append(l)
    episode_rewards.append(r)

# Plot episode length over time
fig1 = plt.figure(figsize=(10,5))
plt.plot(episode_lengths)
plt.xlabel("Episode")
plt.ylabel("Episode Length")
plt.title("Episode Length over Time")
fig1.savefig('episode_lengths.png')
if show:
    plt.show(fig1)
else:
    plt.close(fig1)

# Plot the episode reward over time
fig2 = plt.figure(figsize=(10,5))
plt.plot(episode_rewards)
plt.xlabel("Episode")
plt.ylabel("Episode Reward")
plt.title("Episode Reward over Time")
fig2.savefig('reward.png')
if show:
    plt.show(fig2)
else:
    plt.close(fig2)
