import tensorflow as tf
import numpy as np
import math # or assertions only

class Agent():

    def __init__(self):
        # Build the tensorflow graph
        self.states = tf.placeholder(tf.float32, shape=(None, 6400))
        self.actions = tf.placeholder(tf.int32, shape=(None))
        self.targets = tf.placeholder(tf.float32, shape=(None))

        self.predictions = self.forward_pass(self.states)

        self.outputs_softmax = tf.nn.softmax(self.predictions, name='A3')

        # Define the loss
        neg_log_prob = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=self.predictions, labels=self.actions)
        loss = tf.reduce_mean(neg_log_prob * self.targets)

        # Setup an optimizer in tensorflow to minimize the loss
        self.train_step = tf.train.AdamOptimizer(0.001).minimize(loss)

        #self.estimator = Estimator()

    def forward_pass(self, states):
        """
        Defines structure of network, returns one output for each possible action.
        """
        input_layer = tf.reshape(
            states,
            [-1, 80, 80, 1])

        out_conv1 = tf.layers.conv2d(
            inputs=input_layer,
            filters=4,
            kernel_size=[3, 3],
            padding="same",
            kernel_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.01),
            activation=tf.nn.relu)

        out_conv2 = tf.layers.conv2d(
            inputs=out_conv1,
            filters=4,
            kernel_size=[3, 3],
            padding="same",
            kernel_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.01),
            activation=tf.nn.relu)

        flat_shape = int(out_conv2.shape[1]*out_conv2.shape[2]*out_conv2.shape[3])
        out_conv2_flat = tf.reshape(out_conv2, [-1, flat_shape])

        out_fcon1 = tf.layers.dense(
            inputs=out_conv2_flat,
            units=128,
            kernel_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.01),
            activation=tf.nn.relu)

        out_drop = tf.layers.dropout(out_fcon1, rate=0.5, training=True)

        out_fcon2 = tf.layers.dense(
            inputs=out_drop,
            kernel_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.01),
            units=2,
            activation = None)

        return out_fcon2

    def make_greedy_action(self, sess):
        dict = {self.states: [obersvation]}
        _predictions = sess.run(self.predictions, feed_dict=dict)[0]
        return np.argmax(_predictions)

    def make_action(self, sess, obersvation):
        dict = {self.states: [obersvation]}
        _outputs_softmax = sess.run(self.outputs_softmax, feed_dict=dict)[0]
        # DEBUG ###############
        for prob in _outputs_softmax:
            assert not math.isnan(prob)
        # #####################
        return np.random.choice([0, 1], p=_outputs_softmax)

    def train(self, sess, state_batch, action_batch, target_batch):
        """
        Updates weights of network.
        """
        dict = {
            self.states : state_batch,
            self.actions : action_batch,
            self.targets : target_batch}
        sess.run(self.train_step, feed_dict = dict)

    def reinforce(self, sess, episode):
        if len(episode) == 0:
            return 1

        state_batch = []
        action_batch = []
        target_batch = []
        estimator_target_batch = []

        for t in range(len(episode)):
            v_t = sum(r for i, (s, a, r) in enumerate(episode[t:]))
            state, action, reward = episode[t]
            state_batch.append(state.reshape(-1))
            action_batch.append(action)
            target_batch.append(v_t)

        self.train(sess, state_batch, action_batch, target_batch)

        total_reward = sum(r for i, (s, a, r) in enumerate(episode))
        return total_reward
