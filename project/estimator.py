import tensorflow as tf
#import numpy as np
from utils import Options
import math # DEBUG: for assertions only

opt = Options()

class Estimator():

    def __init__(self):
        # Build the tensorflow graph
        self.states = tf.placeholder(tf.float32, shape=(None, opt.hist_len*opt.state_siz))
        self.targets = tf.placeholder(tf.float32, shape=(None))

        self.predictions = self.forward_pass(self.states)

        # Define the loss
        self.loss = tf.reduce_mean(tf.square(self.targets - self.predictions))

        # Setup an optimizer in tensorflow to minimize the loss
        self.train_step = tf.train.AdamOptimizer(opt.learning_rate).minimize(self.loss)

    def forward_pass(self, states):
        """
        Defines structure of network.
        """
        input_layer = tf.reshape(
            states,
            [-1, opt.pob_siz * opt.cub_siz, opt.pob_siz * opt.cub_siz, opt.hist_len])

        out_conv1 = tf.layers.conv2d(
            inputs=input_layer,
            filters=opt.num_filters,
            kernel_size=[3, 3],
            padding="same",
            kernel_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.01),
            activation=tf.nn.relu)

        out_conv2 = tf.layers.conv2d(
            inputs=out_conv1,
            filters=opt.num_filters,
            kernel_size=[3, 3],
            padding="same",
            kernel_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.01),
            activation=tf.nn.relu)

        flat_shape = int(out_conv2.shape[1]*out_conv2.shape[2]*out_conv2.shape[3])
        out_conv2_flat = tf.reshape(out_conv2, [-1, flat_shape])

        out_fcon1 = tf.layers.dense(
            inputs=out_conv2_flat,
            units=opt.num_units_linear_layer,
            kernel_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.01),
            activation=tf.nn.relu)

        out_drop = tf.layers.dropout(out_fcon1, rate=0.5, training=True)

        out_fcon2 = tf.layers.dense(
            inputs=out_drop,
            kernel_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.01),
            units=1,  # Only one output!
            activation = None)

        predictions = out_fcon2
        return predictions

    def train(self, sess, state_batch, target_batch):
        """
        Updates weights of network.
        """
        dict = {
            self.states : state_batch,
            self.targets : target_batch}
        sess.run(self.train_step, feed_dict = dict)
        # DEBUG ###############
        _loss = sess.run(self.loss, feed_dict = dict)
        assert not math.isnan(_loss)
        # #####################

    def predict(self, sess, state):
        dict = {self.states: [state.reshape(-1)]}
        _prediction = sess.run(self.predictions, feed_dict=dict)[0][0]
        return _prediction
