import tensorflow as tf
import numpy as np
from utils import Options
import math # or assertions only

from estimator import Estimator

opt = Options()

class Agent():

    def __init__(self):
        # Store the history of the observed states
        self.history = np.zeros((opt.hist_len, opt.state_siz))

        # Build the tensorflow graph
        self.states = tf.placeholder(tf.float32, shape=(None, opt.hist_len*opt.state_siz))
        self.actions = tf.placeholder(tf.int32, shape=(None))
        self.targets = tf.placeholder(tf.float32, shape=(None))

        self.predictions = self.forward_pass(self.states)

        self.outputs_softmax = tf.nn.softmax(self.predictions, name='A3')

        # Define the loss
        neg_log_prob = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=self.predictions, labels=self.actions)
        loss = tf.reduce_mean(neg_log_prob * self.targets)

        # Setup an optimizer in tensorflow to minimize the loss
        self.train_step = tf.train.AdamOptimizer(opt.learning_rate).minimize(loss)

        #self.estimator = Estimator()

    def forward_pass(self, states):
        """
        Defines structure of network, returns one output for each possible action.
        """
        input_layer = tf.reshape(
            states,
            [-1, opt.pob_siz * opt.cub_siz, opt.pob_siz * opt.cub_siz, opt.hist_len])

        out_conv1 = tf.layers.conv2d(
            inputs=input_layer,
            filters=opt.num_filters,
            kernel_size=[3, 3],
            padding="same",
            kernel_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.01),
            activation=tf.nn.relu)

        out_conv2 = tf.layers.conv2d(
            inputs=out_conv1,
            filters=opt.num_filters,
            kernel_size=[3, 3],
            padding="same",
            kernel_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.01),
            activation=tf.nn.relu)

        flat_shape = int(out_conv2.shape[1]*out_conv2.shape[2]*out_conv2.shape[3])
        out_conv2_flat = tf.reshape(out_conv2, [-1, flat_shape])

        out_fcon1 = tf.layers.dense(
            inputs=out_conv2_flat,
            units=opt.num_units_linear_layer,
            kernel_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.01),
            activation=tf.nn.relu)

        out_drop = tf.layers.dropout(out_fcon1, rate=0.5, training=True)

        out_fcon2 = tf.layers.dense(
            inputs=out_drop,
            kernel_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.01),
            units=opt.act_num,
            activation = None)

        return out_fcon2

    def append_to_hist(self, obs):
        """
        Add observation to the history.
        """
        for i in range(self.history.shape[0]-1):
            self.history[i, :] = self.history[i+1, :]
        self.history[-1, :] = obs

    def clear_hist(self):
        """
        Clear history if new episode starts.
        """
        self.history[:] = 0

    def make_greedy_action(self, sess):
        dict = {self.states: [self.history.reshape(-1)]}
        _predictions = sess.run(self.predictions, feed_dict=dict)[0]
        return np.argmax(_predictions)

    def make_action(self, sess):
        dict = {self.states: [self.history.reshape(-1)]}
        _outputs_softmax = sess.run(self.outputs_softmax, feed_dict=dict)[0]
        # DEBUG ###############
        for prob in _outputs_softmax:
            assert not math.isnan(prob)
        # #####################
        return np.random.choice([0, 1, 2, 3, 4], p=_outputs_softmax)

    def make_hint_to_goal_action(self, sess, state):
        botx = state.botx
        boty = state.boty

        # up
        dist1 = np.abs(5 - (boty - 1)) + np.abs(5 - botx)
        # down
        dist2 = np.abs(5 - (boty + 1)) + np.abs(5 - botx)
        # left
        dist3 = np.abs(5 - boty) + np.abs(5 - (botx - 1))
        # right
        dist4 = np.abs(5 - boty) + np.abs(5 - (botx + 1))

        dists = [dist1, dist2, dist3, dist4]
        #print(dists)
        action = 1 + np.argmin(dists)
        if np.random.choice([True, False], p=[0.1, 0.9]):
            action = np.random.choice([1, 2, 3, 4])
        return action

    def train(self, sess, state_batch, action_batch, target_batch):
        """
        Updates weights of network.
        """
        dict = {
            self.states : state_batch,
            self.actions : action_batch,
            self.targets : target_batch}
        sess.run(self.train_step, feed_dict = dict)

    def reinforce(self, sess, episode):
        if len(episode) == 0:
            return 1

        state_batch = []
        action_batch = []
        target_batch = []
        estimator_target_batch = []

        for t in range(len(episode)):
            v_t = sum(opt.discount_factor**i * r for i, (s, a, r) in enumerate(episode[t:]))
            state, action, reward = episode[t]
            state_batch.append(state.reshape(-1))
            action_batch.append(action)

            #baseline = self.estimator.predict(sess, state.reshape(-1))
            advantage = v_t #- baseline

            target_batch.append(advantage)
            #estimator_target_batch.append(v_t)

            #self.estimator.train(sess, state_batch, estimator_target_batch)


           # else :
           #     state_next, _, _ = episode[t + 1]
           #     predcited_v_t = self.estimator.predict(sess, state)
           #     predcited_v_t_next = self.estimator.predict(sess, state_next)
           #     advantage = reward + predcited_v_t_next - predcited_v_t

        self.train(sess, state_batch, action_batch, target_batch)

        total_reward = sum(r for i, (s, a, r) in enumerate(episode))
        return total_reward
